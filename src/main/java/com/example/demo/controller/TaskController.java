package com.example.demo.controller;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

@RequestMapping("/nishioze")
@Controller
public class TaskController {

	@Autowired
	TaskService taskService;

	@GetMapping("/newtask")
	public ModelAndView newtask() {
		Task task = new Task();
		task.setDeadline(getTomorrow());
		ModelAndView mav = new ModelAndView();
		mav.addObject("formModel", task);
		mav.setViewName("/newtask");
		return mav;
	}

	@PostMapping("/addtask")
	public ModelAndView addtask(@Validated @ModelAttribute("formModel") Task task, BindingResult result) throws ParseException {
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			task.setDeadline(getTomorrow());
			mav.setViewName("/newtask");
			mav.addObject("formModel", task);
//			taskに新しいdeadlineをsetしているのに、htmlではset以前のフォームで入力された内容が表示されるのはなぜ？
//			mav.addObjectを省いても、フォームの入力内容が保持されているのはaddObjectされた情報が保持される領域に関係している？
			return mav;
		}
		taskService.saveTask(task);
		return new ModelAndView("redirect:/nishioze");
	}

	private Date getTomorrow() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		Date tomorrow = calendar.getTime();
		return tomorrow;
	}
}

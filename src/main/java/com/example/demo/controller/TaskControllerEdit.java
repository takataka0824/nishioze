package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

@RequestMapping("/nishioze")
@Controller
public class TaskControllerEdit {

	@Autowired
	TaskService taskService;

	@GetMapping("/task/{id}/edit")
	public ModelAndView edit(@PathVariable("id") int id) {
		Task task = taskService.findtask(id);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/edit");
		mav.addObject("formModel", task);
		return mav;
	}

	@PutMapping("/task/{id}/update")
	public ModelAndView update(@Validated @ModelAttribute("formModel") Task task, BindingResult result, @PathVariable("id") int id) {
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/edit");
			mav.addObject("formModel", task);
			return mav;
		}

		Task taskOld = taskService.findtask(id);
		task.setId(taskOld.getId());
		task.setCreatedDate(taskOld.getCreatedDate());
		taskService.saveTask(task);
		return new ModelAndView("redirect:/nishioze");
	}
}

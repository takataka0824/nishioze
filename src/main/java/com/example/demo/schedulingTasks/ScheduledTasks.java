package com.example.demo.schedulingTasks;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

//@Component
@EnableScheduling
public class ScheduledTasks {

	TaskService taskService;

//	cron = ss mm hh dd MM YY
	@Scheduled(cron = "0 * * * * *", zone = "Asia/Tokyo")
	public void deleteDoneTasks() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -3);
		Date threeDaysAgo = calendar.getTime();
		List<Task> doneTasks = taskService.findAllTaskDone();

		for(Task task: doneTasks) {
			Date deadline = task.getDeadline();
			if(deadline.before(threeDaysAgo) || deadline.equals(threeDaysAgo)) {
				task.setStatus(2);
				taskService.saveTask(task);
				System.out.println(task.getContent() + "is deleted");
			}
		}
	}

}

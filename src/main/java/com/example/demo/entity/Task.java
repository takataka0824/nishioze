package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "tasks")
@Configuration

public class Task {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	@NotBlank(message = "※必須項目です")
	@Size(max = 20, message = "※20文字以下で入力してください")
	private String content;

	@Column
	private int status;

	@Column
	@Future(message = "※明日以降の日付を入力してください")
	@NotNull(message = "※明日以降の日付を入力してください")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date deadline;

	@Column
	private Date createdDate;

	@Column
	 private Date updatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@PreUpdate
	public void onPreUpdate() {
		setUpdatedDate(new Date());
	}

	@PrePersist
	public void onPrePersist() {
		setCreatedDate(new Date());
		setUpdatedDate(new Date());
	}

}